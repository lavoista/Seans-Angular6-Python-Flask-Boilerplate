FROM alpine:3.7
LABEL Sean Bradley  <seanwasere@gmail.com>

RUN apk add nginx
RUN set -x ; \
  addgroup -g 82 -S www-data ; \
  adduser -u 82 -D -S -G www-data www-data && exit 0 ; exit 1

COPY ./nginx.conf	    /etc/nginx/nginx.conf
#COPY ./localhost.crt	/etc/nginx/localhost.crt
#COPY ./localhost.key	/etc/nginx/localhost.key
COPY ./AngularApp /AngularApp
WORKDIR /AngularApp
